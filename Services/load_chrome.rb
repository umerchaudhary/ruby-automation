require 'selenium-webdriver'

class LoadChrome

  def initialize()
    Selenium::WebDriver::Chrome.driver_path='./../Drivers/chromedriver.exe'
    @driver = Selenium::WebDriver.for :chrome
    @driver.manage.window.maximize
  end

  def get_chrome_driver()
    return @driver
  end

  def close_browser()
    @driver.quit
  end

end