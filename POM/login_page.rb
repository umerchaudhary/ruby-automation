require 'selenium-webdriver'

class LoginPage

  def initialize(driver)
    @driver=driver
  end

  def type_email(email)
    @driver.find_element(:id,'email').send_keys(email)
  end

  def type_password(password)
    @driver.find_element(:id,'passwd').send_keys(password)
  end

  def click_sign_in()
    @driver.find_element(:id,'SubmitLogin').click
  end

  def get_sign_out_link()
    return @driver.find_element(:class,'logout')
  end

  def forgotPassword
    @driver.find_element(:xpath,"//*[text()='Forgot your password?']")
  end

end