require 'selenium-webdriver'


class TShirtsPage


  def initialize(driver)
    @driver=driver
  end

  def firstItem
    @driver.find_element(:xpath,"//ul[@class='product_list grid row']/li[1]")
  end

  def addToCart
    @driver.find_element(:xpath,"//ul[@class='product_list grid row']/li[1]//a[@class='button ajax_add_to_cart_button btn btn-default']")
  end

  def proceedToCheckout
    @driver.find_element(:xpath,"//div[@id='layer_cart']//a[contains(@title,'Proceed to checkout')]")
  end

end