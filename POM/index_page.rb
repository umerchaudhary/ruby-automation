require 'selenium-webdriver'

class IndexPage

  def initialize(driver)
    @driver=driver
  end

  def navigate_url(url)
    @driver.navigate.to url
  end

  def get_sign_in_link
    return @driver.find_element(:class,'login')
  end

  def get_login_form
    return @driver.find_element(:id,'login_form')
  end
  def TShirts
    return @driver.find_element(:xpath,"//*[@class='sf-menu clearfix menu-content sf-js-enabled sf-arrows']/li[3]/a")
  end
end