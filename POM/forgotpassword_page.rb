require 'selenium-webdriver'

class ForgotPassword
  def initialize(driver)
    @driver=driver
  end

  def emailAddress
    @driver.find_element(:xpath,"//input[@class='form-control' and @name='email']")
  end

  def requestReset
    @driver.find_element(:xpath,"//form[@id='form_forgotpassword']//button[@type='submit']")
  end

  def forgotPasswordResult
    @driver.find_element(:xpath,"//*[contains(@class,'alert')]")
  end
  

end