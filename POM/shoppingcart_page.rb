require 'selenium-webdriver'

class ShoppingCart

  def initialize(driver)
    @driver=driver
  end

  def itemDescription
    @driver.find_element(:xpath,"//td[@class='cart_description']")
  end
end